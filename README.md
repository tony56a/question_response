# question_response

Provides responses for relevant questions. To Run:

Question 1:

```
pip install -r requirements.txt
python responses/streaming_processor.py
# Password is:F3RN3T4NDC0C4C0L4
```

Question 2:
```
pip install -r requirements.txt
python responses/posse_processor.py
# Password: f3rn3t
```
