import requests
import json

# Note: this is also available on https://gitlab.com/tony56a/question_response
class PosseProcessor(object):

    def __init__(self, addr):
        self.addr = addr
        self.req_id = None


    def get_hand(self, req_id=None):
        if not req_id:
            get_state_response = requests.post(self.addr)
        else:
            get_state_response = requests.get(self.addr + '/%s' % req_id)
        get_state_response.raise_for_status()

        # Set game ID if not available
        if not self.req_id:
            self.req_id = get_state_response.json()['id']
        return get_state_response.json()

    def submit_hand(self, posse):
        
        get_state_response = requests.post(self.addr + '/%s' % self.req_id, json={'posse': posse})
        get_state_response.raise_for_status()

        return get_state_response.json()

    def get_posse(self, json):
        board = json['board']
        posse = []
        retVal = []
        
        # Get all possible hands that qualify as a posse
        for card in board:
            new_posse = list(posse)
            new_posse.append(card)
            next_cards = list(board)
            next_cards.remove(card)
            self._get_posse(retVal, new_posse, next_cards)
        return [ value for value in retVal if value != None]

    def _get_posse(self, retVal, posse, cards):
        # If a possible posse, check to see if it's valid
        if len(posse) == 3:
            retVal.append(self._validate(posse))
            return

        for card in cards:
            new_posse = list(posse)
            new_posse.append(card)
            next_cards = list(cards)
            next_cards.remove(card)
            self._get_posse(retVal, new_posse, next_cards)

    def _validate(self, posse):

        characters = { 'a': ['a', 'A', 'Ä'], 'o': [ 'o', 'O', 'Ö'], 'u': ['u', 'U', 'Ü'] }
        cases = { 'lower' : ['a','o', 'u'], 'upper': ['A', 'O', 'U'], 'omlut': ['Ä', 'Ö', 'Ü']}

        # Filter all results based on posse characteristics
        prefix_filter = { '-': [item for item in posse if item[0] == '-'], '+': [item for item in posse if item[0] == '+'], '=': [item for item in posse if item[0] == '='] }
        len_filter = { 2: [item for item in posse if len(item) == 2 ], 3: [item for item in posse if len(item) == 3 ], 4: [item for item in posse if len(item) == 4 ] }
        character_filter = { 'a': [item for item in posse if item[1] in characters['a']], 'o': [item for item in posse if item[1] in characters['o']], 'u': [item for item in posse if item[1] in characters['u']] }
        case_filter = {  'lower': [item for item in posse if item[1] in cases['lower']], 'upper': [item for item in posse if item[1] in cases['upper']], 'omlut': [item for item in posse if item[1] in cases['omlut']] }

        # Get length of cards by each filter, and check to see if all results are either the same (length 3 is present somewhere) or are all different(each type only has 1 entry)
        prefix_filter_sizes = [len(filter_content) for filter_content in prefix_filter.values()]
        len_filter_sizes = [len(filter_content) for filter_content in len_filter.values()]
        char_filter_sizes = [len(filter_content) for filter_content in character_filter.values()]
        case_filter_size_set = [len(filter_content) for filter_content in case_filter.values()]

        if not(3 in prefix_filter_sizes or prefix_filter_sizes == [1,1,1]):
            return None
        if not(3 in len_filter_sizes or len_filter_sizes == [1,1,1]):
            return None
        if not (3 in char_filter_sizes or char_filter_sizes == [1,1,1]):
            return None
        if not(3 in case_filter_size_set or case_filter_size_set == [1,1,1]):
            return None 
    
        # If requirements are met, return posse
        return posse

if __name__ == '__main__':

    posse_processor = PosseProcessor("https://enigmatic-hamlet-4927.herokuapp.com/posse")

    json = posse_processor.get_hand()
    while('password' not in json):
        posses = posse_processor.get_posse(json)
        json = posse_processor.submit_hand(posse=posses[0])

    print("Password: %s" % json['password'])
    