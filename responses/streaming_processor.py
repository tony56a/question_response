import requests
import re

# Note: this is also available on https://gitlab.com/tony56a/question_response
class StreamingProcessor(object):

    def __init__(self, addr, chunk_size=1024):
        self.password = ['' for _ in range(chunk_size)]
        self.addr = addr
        self.chunk_size = chunk_size

    def get_char_in_matrix(self, grid, location):
        height = len(grid) - 1

        return grid[height - int(location[1])][int(location[0])]

    def get_password(self):
        get_state_response = requests.get(self.addr, stream=True)
        get_state_response.raise_for_status()
        for chunk in get_state_response.iter_content(chunk_size=self.chunk_size):
            if chunk:
                strings = chunk.decode("utf-8").strip().split('\n')
                password_index = int(strings[0])
                location_regex = r"\[(\d+)\s*\,\s*(\d+)\]"
                location_index = re.findall(location_regex, strings[1])[0]
                grid = strings[2:]

                character = self.get_char_in_matrix(grid, location_index)
                if password_index > (len(self.password)- 1):
                    self.password.append(['' for _ in range(password_index-len(self.password)+1)])
                
                if(self.password[password_index] != ''):
                    return ''.join(self.password)
                else:
                    self.password[password_index] = character
                
    def process_state(self, input_state):
        print(input_state)

if __name__ == '__main__':
    posse_processor = StreamingProcessor("https://enigmatic-plains-7414.herokuapp.com/")
    print('Password is:%s' % posse_processor.get_password())
