FROM python:3.6 as base
ADD . /prep
WORKDIR /prep
RUN pip install -r requirements.txt

FROM base as q2
CMD python responses/posse_processor.py

FROM base as q1
CMD python responses/streaming_processor.py
